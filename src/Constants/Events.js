import photo1 from "../Images/photo1.JPG";
import photo2 from "../Images/photo2.JPG";
import photo3 from "../Images/photo3.JPG";
import photo4 from "../Images/photo4.JPG";
import photo5 from "../Images/photo5.JPG";
import photo6 from "../Images/photo6.JPG";
import photo7 from "../Images/photo7.jpg";
import photo8 from "../Images/photo8.JPG";
import photo9 from "../Images/photo9.JPG";
import photo10 from "../Images/photo10.JPG";
import photo11 from "../Images/photo11.JPG";
import photo12 from "../Images/photo12.JPG";
import photo13 from "../Images/photo13.JPG";

const Events = {
  results: [
    {
      eventId: "1",
      eventName: "Yoga Retreat",
      startDate: "2022-03-28",
      startTime: "08:00",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Yoga" },
        { tagName: "Health & Wellness" },
        { tagName: "Camp, Trip, or Retreat" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Gdańsk",
      image: photo7,
    },
    {
      eventId: "2",
      eventName: "Jazz Concert",
      startDate: "2022-03-23",
      startTime: "20:30",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [{ tagName: "Music" }, { tagName: "Concert" }, { tagName: "Nightlife" }],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Gdańsk",
      image: photo2,
    },
    {
      eventId: "3",
      eventName: "Time Management Training",
      startDate: "2022-03-21",
      startTime: "18:30",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Career" },
        { tagName: "Business & Professional" },
        { tagName: "Class, Training, or Workshop" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Sopot",
      image: photo3,
    },
    {
      eventId: "4",
      eventName: "Sightseeing Tour",
      startDate: "2022-03-20",
      startTime: "08:30",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Nature" },
        { tagName: "Health & Wellness" },
        { tagName: "Camp, Trip, or Retreat" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Sopot",
      image: photo11,
    },
    {
      eventId: "5",
      eventName: "Street Art Tour",
      startDate: "2022-03-19",
      startTime: "12:00",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "City" },
        { tagName: "Art" },
        { tagName: "Camp, Trip, or Retreat" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Sopot",
      image: photo12,
    },
    {
      eventId: "6",
      eventName: "Fishing Trip",
      startDate: "2022-03-27",
      startTime: "05:30",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Nature" },
        { tagName: "Camp, Trip, or Retreat" },
        { tagName: "Class, Training, or Workshop" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Gdynia",
      image: photo13,
    },
    {
      eventId: "7",
      eventName: "Wine Tasting",
      startDate: "2022-03-26",
      startTime: "17:00",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Food & Drinks" },
        { tagName: "Nightlife" },
        { tagName: "Class, Training, or Workshop" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Sopot",
      image: photo1,
    },
    {
      eventId: "8",
      eventName: "Architecture Lecture",
      startDate: "2022-03-23",
      startTime: "17:00",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Career" },
        { tagName: "Art" },
        { tagName: "Class, Training, or Workshop" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Poznań",
      image: photo4,
    },
    {
      eventId: "9",
      eventName: "Business Conference",
      startDate: "2022-03-24",
      startTime: "17:00",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Career" },
        { tagName: "Business & Professional" },
        { tagName: "Class, Training, or Workshop" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Poznań",
      image: photo5,
    },
    {
      eventId: "10",
      eventName: "City Night Tour",
      startDate: "2022-03-20",
      startTime: "19:00",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Nightlife" },
        { tagName: "Food & Drinks" },
        { tagName: "Entertainment" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Poznań",
      image: photo6,
    },
    {
      eventId: "11",
      eventName: "Explore the Countryside",
      startDate: "2022-03-27",
      startTime: "08:00",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Nature" },
        { tagName: "Food & Drinks" },
        { tagName: "Camp, Trip, or Retreat" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Gdańsk",
      image: photo8,
    },
    {
      eventId: "12",
      eventName: "Cheese Tasting",
      startDate: "2022-03-18",
      startTime: "19:00",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Nature" },
        { tagName: "Food & Drinks" },
        { tagName: "Camp, Trip, or Retreat" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Poznań",
      image: photo9,
    },
    {
      eventId: "13",
      eventName: "Agritourism",
      startDate: "2022-03-26",
      startTime: "08:30",
      summary:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehstarterit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      category: [
        { tagName: "Nature" },
        { tagName: "Food & Drinks" },
        { tagName: "Camp, Trip, or Retreat" },
      ],
      latitude: 54.418023,
      longitude: 18.535019,
      city: "Poznań",
      image: photo10,
    },
  ],
};
export default Events;
