import React, { useState } from "react";
import { FormLabel } from "@chakra-ui/react";
import ReactDatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "../date-picker.css";

const CustomDatePicker = (props) => {
  return (
    <span>
      <div>
        <FormLabel>Start Date</FormLabel>
        <ReactDatePicker
          selected={props.startDate}
          onChange={(date) => props.handleChangeStartDate(date)}
        />
      </div>
      <div>
        <FormLabel>End Date</FormLabel>
        <ReactDatePicker
          selected={props.endDate}
          onChange={(date) => props.handleChangeEndDate(date)}
        />
      </div>
    </span>
  );
};
export default CustomDatePicker;
