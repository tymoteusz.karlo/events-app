import React from "react";
import CityPicker from "./CityPicker";
import CategoriesPicker from "./CategoriesPicker";
import CustomDatePicker from "./CustomDatePicker";
import { Filters } from "./styles/Filters.styled";
import { Button } from "@chakra-ui/react";
import { FormLabel } from "@chakra-ui/react";

const FiltersComponent = (props) => {
  return (
    <Filters>
      <CustomDatePicker
        handleChangeStartDate={props.handleChangeStartDate}
        startDate={props.startDate}
        handleChangeEndDate={props.handleChangeEndDate}
        endDate={props.endDate}
      />
      <CategoriesPicker
        categories={props.categories}
        handleChangeCategories={props.handleChangeCategories}
        handleToggleCategories={props.handleToggleFavourites}
        isClicked={props.toggleFavourites}
      />
      <CityPicker
        defaultValue={props.city}
        locationsFilter={props.locationsFilter}
        locations={props.locations}
        handleChangeLocations={props.handleChangeLocations}
      />
      <Button onClick={(e) => props.handleToggleFavourites(e)}>
        {props.isClicked ? (
          <FormLabel> Favourite Events </FormLabel>
        ) : (
          <FormLabel> All Events</FormLabel>
        )}
      </Button>
    </Filters>
  );
};

export default FiltersComponent;
