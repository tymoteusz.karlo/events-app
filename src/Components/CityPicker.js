import React, { useEffect, useState } from "react";
import { Container, FormControl, FormLabel } from "@chakra-ui/react";
import { CreatableSelect, Select } from "chakra-react-select";

const CityPicker = (props) => {
  const [city, setCity] = useState();

  useEffect(() => {
    setCity(
      props.locationsFilter
        ? props.locationsFilter
        : { label: "Sopot", value: "Sopot" }
    );
  }, [props.locationsFilter]);

  return (
    <div>
      <FormControl paddingLeft={4} paddingBottom={4}>
        <FormLabel>Locations</FormLabel>
        <CreatableSelect
          isMulti
          name="Locations"
          options={props.locations}
          placeholder="Cities"
          value={city}
          closeMenuOnSelect={false}
          size="sm"
          onChange={(e) => props.handleChangeLocations(e)}
        />
      </FormControl>
    </div>
  );
};

export default CityPicker;
