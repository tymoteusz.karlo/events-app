import { FormControl, FormLabel } from "@chakra-ui/react";
import { Select } from "chakra-react-select";

const CategoriesPicker = (props) => {
  return (
    <div>
      <FormControl paddingLeft={4} paddingBottom={2}>
        <FormLabel>Categories</FormLabel>
        <Select
          isMulti
          name="Categories"
          options={props.categories}
          placeholder="Categories"
          closeMenuOnSelect={false}
          size="sm"
          onChange={(e) => props.handleChangeCategories(e)}
        />
      </FormControl>
    </div>
  );
};

export default CategoriesPicker;
