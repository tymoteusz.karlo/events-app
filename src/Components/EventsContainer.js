import React, { useState } from "react";
import SingleEvent from "./SingleEvent";
import Events from "../Constants/Events";
import { StyledEventsContainer } from "../Components/styles/EventsContainer.styled";

const EventsContainer = (props) => {
  const locationsFilter = props.locationsFilter
    ? props.locationsFilter.map((x) => x.value)
    : [];
  const categoriesFilter = props.categoriesFilter;
  const favouritesFilter = props.favouritesFilter ? props.favouritesFilter : [];
  const [events, setEvents] = useState();

  !events ? setEvents(Events) : <div></div>;

  return (
    <StyledEventsContainer>
      {events ? (
        events.results
          .filter((x) =>
            props.startDate && props.endDate
              ? props.startDate.getTime() < new Date(x.startDate).getTime() &&
                new Date(x.startDate).getTime() < props.endDate.getTime()
              : true
          )
          .filter(
            locationsFilter
              ? locationsFilter.length
                ? (x) => locationsFilter.includes(x.city)
                : (x) => x
              : (x) => x
          )
          .filter(
            categoriesFilter
              ? categoriesFilter.length
                ? (x) =>
                    x.category
                      .map((x) => x.tagName)
                      .some((r) =>
                        categoriesFilter.indexOf(r) >= 0
                          ? r
                          : console.log("nope")
                      )
                : (x) => x
              : (x) => x
          )
          .map((x) =>
            props.onlyFavourites ? (
              favouritesFilter.indexOf(x.eventId) >= 0 ? (
                <SingleEvent
                  key={x.eventId}
                  favourite={true}
                  event={x}
                  handleChangeFavourites={props.handleChangeFavourites}
                />
              ) : null
            ) : favouritesFilter.indexOf(x.eventId) >= 0 ? (
              <SingleEvent
                key={x.eventId}
                favourite={true}
                event={x}
                handleChangeFavourites={props.handleChangeFavourites}
              />
            ) : (
              <SingleEvent
                key={x.eventId}
                favourite={false}
                event={x}
                handleChangeFavourites={props.handleChangeFavourites}
              />
            )
          )
      ) : (
        <div>No events nearby</div>
      )}
    </StyledEventsContainer>
  );
};

export default EventsContainer;
