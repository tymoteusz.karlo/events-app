import styled from "styled-components";

export const StyledCustomPopover = styled.ul`
  background: ${({ theme }) => theme.colors.body};

  & > button {
    background: ${({ theme }) => theme.colors.body};
    width: 300px;
  }
  & > div > section {
    border-radius: 5px;
  }
  & > div > section > header {
    background: ${({ theme }) => theme.colors.footer};
  }
`;
