import styled from "styled-components";

export const Filters = styled.div`
  display: grid;
  grid-template-columns: auto / 1fr 64px 80px 64px 1fr

  margin: 5px;
  margin-bottom: 20px;
  background-position: center center;
  background-size: cover;
  text-align: center;

  border-radius: 5px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);

  & > button {
  padding-top: 5px;
  margin: 10px;
  background: ${({ theme }) => theme.colors.body};
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.7);
  };

 &:hover {
  & > button {
    background: ${({ theme }) => theme.colors.footer};
    }
}
& > div > div > label {
  padding-left: 10px;
}

& > span > div > label {
  padding-left: 15px;
}

  @media (max-width: 500px) {
  & > div {
    display: grid;
    grid-template-columns: auto;
    margin: 5px;
  }
}

  @media (min-width: 500px) {
    & > span {
      display: grid;
      grid-template-columns: auto auto;  
      margin: 5px;
    }
}

  & > span > div {
      margin: 5px;
  }
`;
