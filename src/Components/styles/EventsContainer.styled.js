import styled from "styled-components";

export const StyledEventsContainer = styled.ul`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  list-style: none;
  padding: 0;
  font-weight: 500;
`;
