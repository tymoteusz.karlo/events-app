import styled from "styled-components";

export const StyledHeader = styled.header`
  background: ${({ theme }) => theme.colors.header};
  font-size: 25px;
  display: flex;
  padding-top: 10px;
  font-weight: 300;
  height: 60px;
  justify-content: center;
`;
