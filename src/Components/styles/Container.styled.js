import styled from "styled-components";

export const StyledContainer = styled.div`
  width: 300px;
  display: flex;
  flex: 0 0 300px;
  flex-direction: column;
  margin: 5px;
  margin-bottom: 20px;
  overflow: hidden;
  background-position: center center;
  background-size: cover;
  border-radius: 5px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
`;

export const Information = styled.div`
  display: flex;
  justify-content: center;
  padding-top: 0.25rem;
  padding-left: 1rem;
  background-color: ${({ theme }) => theme.colors.header};

  & > h1 {
    padding-bottom: 0.2rem;
    font-weight: 400;
    font-size: 1.2em;
  }
  & > button {
    margin-bottom: 5px;
    background: content-box;
    height: auto;

    &:hover {
      background: content-box;
    }
  }

  & > button > svg {
    margin-top: 3px;
  }
`;

export const MoreInformation = styled.div`
  padding-left: 0.75rem;
  padding-right: 0.75rem;
  background-color: ${({ theme }) => theme.colors.header};

  & > p {
    padding-top: 0.5rem;
    padding-bottom: 0.75rem;
    font-weight: 600;
  }
  & > div {
    height: 65px;
    padding-top: 0.25rem;
    padding-bottom: 0.5rem;
  }
  & > div > span {
    display: inline-flex;
    padding-left: 0.75rem;
    padding-right: 0.75rem;
    justify-content: center;
    display: inline-block;
    background-color: rgb(229 231 235);
    border-radius: 15px;
    margin-right: 0.5rem;
    font-size: 0.875rem;
    line-height: 1.25rem;
    font-weight: 600;
    color: rgb(55 65 81);
  }
`;

export const Summary = styled.div`
font-size: 0.875rem;
padding: 20px;
opacity: 0;
  &:hover { opacity 1;
    background-color: ${({ theme }) => theme.colors.header};
  }
`;
