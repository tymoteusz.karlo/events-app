import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverFooter,
  PopoverArrow,
  PopoverCloseButton,
  ButtonGroup,
  Button,
} from "@chakra-ui/react";
import { StyledCustomPopover } from "../Components/styles/CustomPopover.styled";

const CustomPopover = () => {
  return (
    <StyledCustomPopover>
      <Popover placement="top" closeOnBlur={false}>
        <PopoverTrigger>
          <Button>Sign Up!</Button>
        </PopoverTrigger>
        <PopoverContent>
          <PopoverHeader>Sign Up</PopoverHeader>
          <PopoverArrow />
          <PopoverCloseButton />
          <PopoverBody>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore.
          </PopoverBody>
          <PopoverFooter
            border="2"
            alignItems="center"
            justifyContent="space-between"
            pb={4}
          >
            <ButtonGroup size="sm">
              <Button colorScheme="red">Buy ticket</Button>
              <Button colorScheme="blackAlpha">More info</Button>
            </ButtonGroup>
          </PopoverFooter>
        </PopoverContent>
      </Popover>
    </StyledCustomPopover>
  );
};

export default CustomPopover;
