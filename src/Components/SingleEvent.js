import React from "react";
import {
  StyledContainer,
  Information,
  MoreInformation,
  Summary,
} from "./styles/Container.styled";
import { AddIcon, StarIcon } from "@chakra-ui/icons";
import { Button } from "@chakra-ui/react";
import CustomPopover from "./CustomPopover";

const SingleEvent = (props) => {
  return (
    <StyledContainer
      style={{
        backgroundImage: `url(${props.event.image})`,
      }}
    >
      <Information>
        <h1 key={props.event.eventId}>{props.event.eventName}</h1>
        <Button
          onClick={() => props.handleChangeFavourites(props.event.eventId)}
        >
          {props.favourite ? <StarIcon /> : <AddIcon />}
        </Button>
      </Information>
      <MoreInformation>
        <p>
          {props.event.city} {props.event.startDate} {props.event.startTime}
        </p>
        <div>
          {props.event.category.map((category) => (
            <span key={category.tagName + props.event.eventId}>
              {category.tagName}
            </span>
          ))}
        </div>
      </MoreInformation>
      <Summary>{props.event.summary}</Summary>
      <CustomPopover> </CustomPopover>
    </StyledContainer>
  );
};

export default SingleEvent;
