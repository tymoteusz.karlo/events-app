import React, { useState } from "react";
import { ThemeProvider } from "styled-components";
import EventsContainer from "./Components/EventsContainer";
import Events from "./Constants/Events";
import FiltersComponent from "./Components/Filters";
import { StyledHeader } from "./Components/styles/Header.styled";
import "./App.css";

const theme = {
  colors: {
    header: "rgba(238, 238, 238, 0.6)",
    headerOpaque: "rgba(238, 238, 238, 0.7)",
    body: "rgba(204, 204, 204)",
    footer: "rgba(153, 153, 153, 0.7)",
    dark: "rgba(102, 102, 102)",
  },
};

function App() {
  const [categories, setCategoriesList] = useState();
  const [categoriesFilter, setCategoriesFilter] = useState();
  const [locations, setLocationsList] = useState();
  const [favourites, setFavourites] = useState();
  const [toggleFavourites, setToggleFavourites] = useState();
  const [startDate, setStartDateFilter] = useState();
  const [endDate, setEndDateFilter] = useState();
  let arrayCategories = [];
  let arrayLocations = [];
  let arrayCategoriesObjects = [];
  let arrayLocationsObjects = [];

  Events ? (
    Events.results.map((x) =>
      x.category.map((y) =>
        arrayCategories.indexOf(y.tagName) === -1
          ? arrayCategories.push(y.tagName)
          : false
      )
    )
  ) : (
    <div></div>
  );

  Events ? (
    Events.results.map((x) =>
      arrayLocations.indexOf(x.city) === -1 ? (
        arrayLocations.push(x.city)
      ) : (
        <div></div>
      )
    )
  ) : (
    <div></div>
  );

  arrayCategories.map((category) =>
    arrayCategoriesObjects.push({ label: category, value: category })
  );
  arrayLocations.map((city) =>
    arrayLocationsObjects.push({ label: city, value: city })
  );
  !categories ? setCategoriesList(arrayCategoriesObjects) : <div></div>;

  !locations ? setLocationsList(arrayLocationsObjects) : <div></div>;

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((position) => {
      const [latitude, longitude] = [
        position.coords.latitude,
        position.coords.longitude,
      ];
      let city;

      var axios = require("axios").default;
      var options = {
        method: "GET",
        url: "https://geocodeapi.p.rapidapi.com/GetNearestCities",
        params: {
          latitude: latitude.toString(),
          longitude: longitude.toString(),
          range: "0",
        },
        headers: {
          "x-rapidapi-host": "geocodeapi.p.rapidapi.com",
          "x-rapidapi-key":
            "db8a70b69fmshffba79f65d68ef8p176811jsne71e16661dcf",
        },
      };
      if (!localStorage.getItem("city")) {
        axios
          .request(options)
          .then(function (response) {
            city = response.data;
            return localStorage.setItem("city", response.data[0].City);
          })
          .catch(function (error) {
            console.error(error);
            return (city = "No data");
          });
      }
    });
  }

  const handleToggleFavourites = () => {
    setToggleFavourites(toggleFavourites ? false : true);
  };
  const handleChangeCategories = (e) => {
    let stateCategories = [];
    e.map((x) => stateCategories.push(x.value));
    setCategoriesFilter(stateCategories);
  };
  const [locationsFilter, setLocationsFilter] = useState([
    {
      label: localStorage.getItem("city"),
      value: localStorage.getItem("city"),
    },
  ]);

  let stateLocations = [];
  const handleChangeLocations = (e) => {
    e.map((x) => stateLocations.push(x));
    setLocationsFilter(stateLocations);
  };

  let favouritesList = localStorage.getItem("favouritesList")
    ? localStorage.getItem("favouritesList").split(",")
    : null;
  const handleChangeFavourites = (e) => {
    localStorage.getItem("favouritesList")
      ? localStorage.getItem("favouritesList").split(",")
        ? localStorage.getItem("favouritesList").split(",").includes(e)
          ? (favouritesList = localStorage
              .getItem("favouritesList")
              .split(",")
              .filter((item) => item !== e))
          : (favouritesList = [
              ...localStorage.getItem("favouritesList").split(","),
              e,
            ])
        : (favouritesList = [e])
      : (favouritesList = [e]);
    setFavourites(favouritesList);
    localStorage.setItem("favouritesList", favouritesList);
  };

  const handleChangeStartDate = (e) => {
    setStartDateFilter(new Date(e));
  };
  const handleChangeEndDate = (e) => {
    setEndDateFilter(new Date(e));
  };

  return (
    <ThemeProvider theme={theme}>
      <StyledHeader> EVENTS IN YOUR NEIGHBOURHOOD </StyledHeader>
      <div className="App">
        <FiltersComponent
          categories={categories}
          handleChangeCategories={handleChangeCategories}
          handleToggleFavourites={handleToggleFavourites}
          isClicked={toggleFavourites}
          locations={locations}
          locationsFilter={locationsFilter}
          handleChangeLocations={handleChangeLocations}
          city={localStorage.getItem("city")}
          startDate={startDate ? startDate : new Date()}
          handleChangeStartDate={handleChangeStartDate}
          endDate={endDate ? endDate : new Date()}
          handleChangeEndDate={handleChangeEndDate}
        />
        <EventsContainer
          locationsFilter={locationsFilter}
          categoriesFilter={categoriesFilter}
          favouritesFilter={favouritesList}
          handleChangeFavourites={handleChangeFavourites}
          onlyFavourites={toggleFavourites}
          startDate={startDate}
          endDate={endDate}
        />
      </div>
    </ThemeProvider>
  );
}

export default App;
